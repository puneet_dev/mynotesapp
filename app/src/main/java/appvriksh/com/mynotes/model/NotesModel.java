package appvriksh.com.mynotes.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by reflex on 1/11/17.
 */

public class NotesModel implements Parcelable {

    public NotesModel(){

    }
    public NotesModel(String notesTitle, String notesBody, String notesTime){
        this.notesTitle = notesTitle;
        this.notesBody = notesBody;
        this.notesTime = notesTime;
        this.notesId = notesTime;
    }

    public NotesModel (Parcel parcel) {
        this.notesTitle = parcel.readString();
        this.notesBody = parcel.readString();
        this.notesTime = parcel.readString();
        this.notesId = parcel.readString();
    }

    public String notesTitle;
    public String notesId;

    public String getNotesId() {
        return notesId;
    }

    public void setNotesId(String notesId) {
        this.notesId = notesId;
    }

    public String getNotesBody() {
        return notesBody;
    }

    public void setNotesBody(String notesBody) {
        this.notesBody = notesBody;
    }

    public String getNotesTime() {
        return notesTime;
    }

    public void setNotesTime(String notesTime) {
        this.notesTime = notesTime;
    }

    public String getNotesTitle() {
        return notesTitle;
    }

    public void setNotesTitle(String notesTitle) {
        this.notesTitle = notesTitle;
    }

    public String notesBody;
    public String notesTime;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(notesTitle);
        dest.writeString(notesBody);
        dest.writeString(notesTime);
        dest.writeString(notesId);
    }

    // Method to recreate a Question from a Parcel
    public static Creator<NotesModel> CREATOR = new Creator<NotesModel>() {

        @Override
        public NotesModel createFromParcel(Parcel source) {
            return new NotesModel(source);
        }

        @Override
        public NotesModel[] newArray(int size) {
            return new NotesModel[size];
        }

    };
}
