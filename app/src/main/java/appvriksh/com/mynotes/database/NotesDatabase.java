package appvriksh.com.mynotes.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

import java.util.ArrayList;

import appvriksh.com.mynotes.model.NotesModel;

/**
 * Created by reflex on 1/11/17.
 */

public class NotesDatabase {
    DbHelper dbHelper;
    SQLiteDatabase sqLiteDatabase;

    public NotesDatabase(Context context) {
        dbHelper = new DbHelper(context);
        sqLiteDatabase = dbHelper.getWritableDatabase();
    }

    public long dataInsert(String notesTitle, String notesBody, String notesTime, String tableName) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put(dbHelper.NOTES_TITLE, notesTitle);
        contentValues.put(dbHelper.NOTES_BODY, notesBody);
        contentValues.put(dbHelper.NOTES_TIME, notesTime);

        long id = db.insert(tableName, null, contentValues);
        return id;
    }

    public long dataUpdate(String notesId, String notesTitle, String notesBody, String notesTime, String tableName) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put(dbHelper.NOTES_TITLE, notesTitle);
        contentValues.put(dbHelper.NOTES_BODY, notesBody);
        contentValues.put(dbHelper.NOTES_TIME, notesTime);

        int id = db.update(tableName, contentValues, "_id =?", new String[]{notesId});
        return id;
    }

    public ArrayList<NotesModel> getNotesArrayList(String tableName, String where, String args){
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String[] columns = {dbHelper.UID, dbHelper.NOTES_TITLE, dbHelper.NOTES_BODY, dbHelper.NOTES_TIME};
        String  whereClause="";
        String[] whereArgs= null;
        Cursor cursor = null;
        if(!where.isEmpty() && !args.isEmpty()){
            whereClause = where + " = ?";
            whereArgs = new String[] {
                    args.toLowerCase()
            };
            cursor = db.rawQuery("select * from " + tableName + " where " + where + "='" + args + "'" , null);
        }else{
            cursor = db.query(tableName, columns, null, null, null, null, null);
        }
        ArrayList<NotesModel> notesArraylist = new ArrayList<>();
        NotesModel notesModel;

        if(cursor.getCount()>0){
            cursor.moveToFirst();
            do{
                String notesId = cursor.getString(0);
                String notesTitle = cursor.getString(1);
                String notesBody = cursor.getString(2);
                String notesTime = cursor.getString(3);

                notesModel = new NotesModel();
                notesModel.setNotesId(notesId);
                notesModel.setNotesTitle(notesTitle);
                notesModel.setNotesBody(notesBody);
                notesModel.setNotesTime(notesTime);

                notesArraylist.add(notesModel);
            }
            while (cursor.moveToNext());
            cursor.close();
            db.close();
        }
        return notesArraylist;
    }

    public class DbHelper extends SQLiteOpenHelper {

        Context context;
        private static final String DATABASE_NAME = "notesdb";

        private static final String NOTES_TABLE_NAME = "my_notes";


        private static final int DATBASE_VERSION = 1;

        private static final String UID = "_id";
        private static final String NOTES_TITLE = "notes_title";
        private static final String NOTES_BODY = "notes_body";
        private static final String NOTES_TIME = "time";

        private static final String CREATE_TABLE_NOTES = "CREATE TABLE " + NOTES_TABLE_NAME + " (" + UID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + NOTES_TITLE + " TEXT, " + NOTES_BODY + " TEXT, " + NOTES_TIME + " TEXT );";

        private static final String DROP_TABLE1 = "DROP TABLE  IF EXISTS " + NOTES_TABLE_NAME;


        public DbHelper(Context context) {
            super(context, DATABASE_NAME, null, DATBASE_VERSION);
            this.context = context;
        }

        @Override
        public void onCreate(SQLiteDatabase sqLiteDatabase) {
            try {
                sqLiteDatabase.execSQL(CREATE_TABLE_NOTES);
                Toast.makeText(context, "oncreate table ", Toast.LENGTH_LONG).show();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
            sqLiteDatabase.execSQL(DROP_TABLE1);
            onCreate(sqLiteDatabase);
            Toast.makeText(context, "onUpgrade table ", Toast.LENGTH_LONG).show();
        }
    }
}
