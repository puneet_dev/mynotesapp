package appvriksh.com.mynotes.utils;

/**
 * Created by reflex on 1/11/17.
 */

public class Constants {
    private Constants(){

    }
    private static Constants singleton = new Constants();

    public static Constants getInstance(){
        return singleton;
    }

    public static final String NOTES_TABLE_NAME = "my_notes";


}
