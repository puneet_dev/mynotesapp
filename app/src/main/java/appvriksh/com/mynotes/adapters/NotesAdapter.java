package appvriksh.com.mynotes.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import appvriksh.com.mynotes.R;
import appvriksh.com.mynotes.model.NotesModel;

/**
 * Created by reflex on 1/11/17.
 */

public class NotesAdapter extends RecyclerView.Adapter<NotesAdapter.MyViewHolder>{
    private ArrayList<NotesModel> notesArrayList = new ArrayList<>();
    private Context mcontext;
    NotesModel notesModel =  new NotesModel();
    private OnItemClickListener mItemClickListener;

    public NotesAdapter(ArrayList<NotesModel> notesArrayList,Context mcontext) {
        this.notesArrayList=notesArrayList;
        this.mcontext=mcontext;
    }

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.notes_row, parent, false);
        return new NotesAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        try{
            notesModel=notesArrayList.get(position);

            String notesTitle= notesModel.getNotesTitle();
            String notesBody= notesModel.getNotesBody();

            Typeface headerTypeface = Typeface.createFromAsset(mcontext.getAssets(), "fonts/monos.ttf");
            Typeface bodyTypeface = Typeface.createFromAsset(mcontext.getAssets(), "fonts/Lato-Light.ttf");

            holder.notesHeaderText.setTypeface(headerTypeface);
            holder.notesBodyText.setTypeface(bodyTypeface);
            setData(holder.notesHeaderText,notesTitle);
            setData(holder.notesBodyText,notesBody);

            holder.notesLayout.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    Intent sendIntent = new Intent();

                    sendIntent.setAction(Intent.ACTION_SEND);
                    sendIntent.putExtra(Intent.EXTRA_TEXT, notesArrayList.get(position).getNotesTitle().toString() + "\n"+ notesArrayList.get(position).getNotesBody());
                    sendIntent.setType("text/plain");
                    mcontext.startActivity(sendIntent);
                    return true;
                }
            });

            holder.notesLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mItemClickListener.onItemClick(position);
                }
            });
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public void setData(TextView tv,String viewString){
        if(viewString!=null && !viewString.isEmpty())
            tv.setText(viewString);
    }

    @Override
    public int getItemCount() {
        return notesArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView notesBodyText,notesHeaderText;
        private LinearLayout notesLayout;

        public MyViewHolder(View view) {
            super(view);
            notesHeaderText= (TextView) view.findViewById(R.id.notes_header_text);
            notesBodyText= (TextView) view.findViewById(R.id.notes_body_text);
            notesLayout = (LinearLayout) view.findViewById(R.id.notes_layout);
        }
    }
}
