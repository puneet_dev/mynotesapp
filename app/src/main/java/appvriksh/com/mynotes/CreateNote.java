package appvriksh.com.mynotes;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;

import appvriksh.com.mynotes.database.NotesDatabase;
import appvriksh.com.mynotes.model.NotesModel;
import appvriksh.com.mynotes.utils.Constants;

public class CreateNote extends AppCompatActivity implements View.OnClickListener {
    Button submitNote;
    EditText noteHeader, noteBody;
    NotesDatabase notesDatabase;
    String notesId="";
    ArrayList<NotesModel> notesArrayList= new ArrayList<>();
    boolean flag = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_note);
        noteHeader = (EditText) findViewById(R.id.header_edit);
        noteBody = (EditText) findViewById(R.id.body_edit);
        submitNote = (Button) findViewById(R.id.submit_note);
        notesDatabase = new NotesDatabase(this);

        if(getIntent().getAction()!=null){
            onNewIntent(getIntent());
        }
        if(!flag){
            if(getIntent().getExtras()!=null){
                notesId = (String) getIntent().getExtras().get("notesId");
                if(!notesId.isEmpty()){
                    notesArrayList= notesDatabase.getNotesArrayList(Constants.NOTES_TABLE_NAME, "_id", notesId);
                    noteHeader.setText(notesArrayList.get(0).getNotesTitle());
                    noteBody.setText(notesArrayList.get(0).getNotesBody());
                }
            }
        }
        submitNote.setOnClickListener(this);
    }

    protected void onNewIntent(Intent intent) {
        flag = true;
        String action = intent.getAction();
        String data = intent.getDataString();
        if (Intent.ACTION_VIEW.equals(action) && data != null) {
            String notesTitle = data.substring(data.lastIndexOf("/") + 1);
            if(!notesTitle.isEmpty()){
                notesArrayList= notesDatabase.getNotesArrayList(Constants.NOTES_TABLE_NAME, "notes_title", notesTitle);
                if(notesArrayList.size()>0){
                    noteHeader.setText(notesArrayList.get(0).getNotesTitle());
                    noteBody.setText(notesArrayList.get(0).getNotesBody());
                    noteHeader.setEnabled(false);
                    noteBody.setEnabled(false);
                    submitNote.setVisibility(View.GONE);
                }else{
                    Toast.makeText(this, "No Notes Found", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch(id){
            case R.id.submit_note:{
                if(!notesId.isEmpty()){
                    String noteHeaderString = noteHeader.getText().toString().trim();
                    String noteBodyString = noteBody.getText().toString().trim();
                    if(!noteHeaderString.isEmpty()){
                        long dataUpdatedId =notesDatabase.dataUpdate(notesId, noteHeaderString, noteBodyString, "1234", Constants.NOTES_TABLE_NAME);
                        if(dataUpdatedId==1){
                            Toast.makeText(CreateNote.this, "Note Updated Successfully", Toast.LENGTH_SHORT).show();
                        }
                    }else{
                        Toast.makeText(CreateNote.this, "Fill Data Properly", Toast.LENGTH_SHORT).show();
                    }
                }else{
                    String noteHeaderString = noteHeader.getText().toString().trim();
                    String noteBodyString = noteBody.getText().toString().trim();

                    if(!noteHeaderString.isEmpty()){
                        long dataInsertedId =notesDatabase.dataInsert(noteHeaderString, noteBodyString, "1234", Constants.NOTES_TABLE_NAME);
                        if(dataInsertedId>0){
                            Toast.makeText(CreateNote.this, "Note Saved Successfully", Toast.LENGTH_SHORT).show();
                            noteHeader.setText("");
                            noteBody.setText("");
                        }
                    }else{
                        Toast.makeText(CreateNote.this, "Fields can't be blank", Toast.LENGTH_SHORT).show();
                    }
                }
                break;
            }
        }
    }
}
