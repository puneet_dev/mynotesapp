package appvriksh.com.mynotes;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.ArrayList;

import appvriksh.com.mynotes.adapters.NotesAdapter;
import appvriksh.com.mynotes.database.NotesDatabase;
import appvriksh.com.mynotes.model.NotesModel;
import appvriksh.com.mynotes.utils.Constants;

public class MainActivity extends AppCompatActivity {
    Button createNoteButton;
    RecyclerView mNotesRecyclerview;
    private ProgressBar mprogressBar;
    NotesDatabase notesDatabase=null;
    ArrayList<NotesModel> notesArrayList;
    NotesAdapter notesAdapter;
    private FloatingActionButton fab;
    private ImageView fabImage;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViews();
        fabImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent (MainActivity.this, CreateNote.class);
                startActivity(intent);
            }
        });
        try{
            getNotesListFromDb();
        }catch(NullPointerException e){
            e.printStackTrace();
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }

    public void initViews() {
        mprogressBar = (ProgressBar)findViewById(R.id.progress_bar);
        mprogressBar.setVisibility(View.GONE);
        mNotesRecyclerview = (RecyclerView) findViewById(R.id.notes_recyclerview);
        fabImage = (ImageView) findViewById(R.id.fab_image);
        fab = (FloatingActionButton) findViewById(R.id.fab);
        notesDatabase = new NotesDatabase(this);
        notesArrayList = new ArrayList<>();
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 2);
        mNotesRecyclerview.setLayoutManager(mLayoutManager);
        mNotesRecyclerview.setItemAnimator(new DefaultItemAnimator());
    }

    public void getNotesListFromDb(){
        new AsyncTask<Void, Void, ArrayList<NotesModel>>() {

            @Override
            protected void onPreExecute() {
                mprogressBar.setVisibility(View.VISIBLE);
            }

            @Override
            protected ArrayList<NotesModel> doInBackground(Void... voids) {
                ArrayList<NotesModel> notesArrayList = new ArrayList<>();
                notesArrayList= notesDatabase.getNotesArrayList(Constants.NOTES_TABLE_NAME,"","");
                return notesArrayList;
            }

            @Override
            protected void onPostExecute(final ArrayList<NotesModel> notesArrayList) {
                mprogressBar.setVisibility(View.GONE);
                if(notesArrayList!=null && notesArrayList.size()>0){
                    notesAdapter = new NotesAdapter(notesArrayList, MainActivity.this);
                    mNotesRecyclerview.setAdapter(notesAdapter);

                    notesAdapter.setOnItemClickListener(new NotesAdapter.OnItemClickListener() {
                        @Override
                        public void onItemClick(int position) {
                            String notesId = notesArrayList.get(position).getNotesId();
                            Intent intent = new Intent (MainActivity.this, CreateNote.class);
                            intent.putExtra("notesId", notesId);
                            startActivity(intent);
                        }
                    });
                }else{
                    Toast.makeText(MainActivity.this, "No Notes Available", Toast.LENGTH_SHORT).show();
                }
            }
        }.execute();
    }

    @Override
    protected void onResume() {
        super.onResume();
        try{
            getNotesListFromDb();
        }catch(NullPointerException e){
            e.printStackTrace();
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }
}
